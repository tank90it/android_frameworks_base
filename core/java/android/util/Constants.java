package android.util;

import android.util.Base64;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class Constants {
    public static String AndroidSeri = getInfo("AndroidSeri");
    public static String BOOTLOADER = getInfo("BOOTLOADER");
    public static String BSSID = getInfo("BSSID");
    public static String BaseBand = getInfo("BaseBand");
    public static String BuildDate = getInfo("BuildDate");
    public static String CountryCode = getInfo("CountryCode");
    public static String DPI = getInfo("DPI");
    public static String DateUTC = getInfo("DateUTC");
    public static String GLRenderer = getInfo("GLRenderer");
    public static String GLVendor = getInfo("GLVendor");
    public static String IMEI = getInfo("IMEI");
    public static String PhoneNumber = getInfo("PhoneNumber");
    public static String SimOperator = getInfo("SimOperator");
    public static String SimOperatorName = getInfo("SimOperatorName");
    public static String SimSerial = getInfo("SimSerial");
    public static String SubscriberId = getInfo("SubscriberId");
    public static String UserAgent = getInfo("UserAgent");
    public static String androidID = getInfo("androidID");
    public static String board = getInfo("board");
    public static String hardware = getInfo("hardware");
    public static String platform = getInfo("platform");
    public static String brand = getInfo("brand");
    public static String btags = getInfo("btags");
    public static String btype = getInfo("btype");
    public static String buser = getInfo("buser");
    public static String debug = getInfo("debug");
    public static String description = getInfo("description");
    public static String displayId = getInfo("displayId");
    public static String fingerprint = getInfo("fingerprint");
    public static String host = getInfo("host");
    public static String id = getInfo("id");
    public static String incremantal = getInfo("incremantal");
    public static String ipAddress = getInfo("ipAddress");
    public static String lat = getInfo("lat");
    public static String lon = getInfo("lon");
    public static String model = getInfo("model");
    public static String radio = getInfo("radio");
    public static String release = getInfo("release");
    public static String sdk = getInfo("sdk");
    public static String security_patch = getInfo("security_patch");
    public static String serial = getInfo("serial");
    public static String wifiMac = getInfo("wifiMac");
    public static String wifiName = getInfo("wifiName");
    // extra info
    public static String osName = getInfo("osName");
    public static String osNetwork = getInfo("osNetwork");
    public static String osRelease = getInfo("osRelease");
    public static String osVersion = getInfo("osVersion");
    public static String osMachine = getInfo("osMachine");


    private static final String KEY = "winteam@tank@key";
    private static final String IV = "winteam2020@tank";

    public static String decrypt(String textToDecrypt) throws Exception {
        IvParameterSpec iv = new IvParameterSpec(IV.getBytes("UTF-8"));
        SecretKeySpec secretKeySpec = new SecretKeySpec(KEY.getBytes("UTF-8"), "AES");

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, iv);

        byte[] original = cipher.doFinal(Base64.decode(textToDecrypt, Base64.DEFAULT));

        return new String(original);
    }

    public static String getInfo(String str) {
        String str2 = "";
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("/system/vendor/Utils/" + str)));
            String readLine = bufferedReader.readLine();
            if (readLine != null) {
                try {
                    str2 = decrypt(readLine);
                } catch (Exception e2) {
                    return readLine;
                }
            } else {
                str2 = readLine;
            }
            bufferedReader.close();
            return str2;
        } catch (Exception e3) {
            return str2;
        }
    }

    public static ArrayList<String> getPackageRemove() {
        ArrayList<String> arrayList = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(new File("/data/local/tmp/pkg")));
            while (true) {
                String readLine = bufferedReader.readLine();
                if (readLine == null) {
                    break;
                }
                arrayList.add(readLine);
            }
            bufferedReader.close();
        } catch (Exception e) {
        }
        return arrayList;
    }
}